package com.microworld.thingsledger.thingsledger.tasklet;

import com.microworld.thingsledger.thingsledger.Contract.ThingsLedgerToken;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;

import java.math.BigInteger;

public class TokenInfo {

    public void doTokenInfo() throws Exception {
        String userAddress ="0xf2DB5E17607943afD1bcD4c2716071e25779c7B7";
        Web3j web3j = Admin.build(new HttpService("https://rpc.testnet.ludos.one"));
        String contractAddress="0xcf2095370ba125299afe0a134dda50530e6b18a9";
        Credentials credentials = Credentials.create(userAddress);
        ThingsLedgerToken thingsLedgerToken = ThingsLedgerToken.load(contractAddress,web3j,credentials,new BigInteger("0"), Contract.GAS_LIMIT);
        System.out.println("amount:"+thingsLedgerToken.totalSupply().send());
    }

    public static void main(String args[]) throws Exception {
        new TokenInfo().doTokenInfo();
    }
}
