package com.microworld.thingsledger.thingsledger.tasklet;

import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.response.TransactionReceiptProcessor;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

public class TransactionSon  extends TransactionManager {

    protected TransactionSon(TransactionReceiptProcessor transactionReceiptProcessor, String fromAddress) {
        super(transactionReceiptProcessor, fromAddress);
    }

    protected TransactionSon(Web3j web3j, String fromAddress) {
        super(web3j, fromAddress);
    }

    protected TransactionSon(Web3j web3j, int attempts, long sleepDuration, String fromAddress) {
        super(web3j, attempts, sleepDuration, fromAddress);
    }

    @Override
    public EthSendTransaction sendTransaction(BigInteger gasPrice, BigInteger gasLimit, String to, String data, BigInteger value, boolean constructor) throws IOException {
        return null;
    }

    @Override
    public String sendCall(String to, String data, DefaultBlockParameter defaultBlockParameter) throws IOException {
        return null;
    }
}
