package com.microworld.thingsledger.thingsledger.tasklet;

import com.microworld.thingsledger.thingsledger.Contract.MWSmartLock;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.utils.Convert;

import java.math.BigInteger;

public class ContractTransaction {

    public static  void main(String[] args) throws Exception {
        new ContractTransaction().doContractTransaction();
    }

    /**
     *
     */
    private void doContractTransaction() throws Exception {
//        String fileName = WalletUtils.generateNewWalletFile(
//                "mwthingsledger",
//                ResourceUtils.getFile("classpath:privateKey/loginKey"));
        Admin web3j = Admin.build(new HttpService("https://rpc.testnet.ludos.one"));
        String ContractAddress = "0x527270e894e85774c9766eb8dd9e7918857d88b6";
        String address = "0xfba860f91eb2e45008e7b8a3d2cc57dfe8bc6a0f";
        String toAddress = "0xaf1522C811A4aD74332D89DEA9e3615c30d007B9";
        BigInteger gasPrice = new BigInteger("0");
        BigInteger gasLimit = Contract.GAS_LIMIT;
        BigInteger value = new BigInteger(String.valueOf(Convert.toWei("1", Convert.Unit.ETHER)));
        Credentials credentials = WalletUtils.loadCredentials(
                "mwthingsledger",
                "src/main/resources/privateKey/loginKey");
        //contract
        MWSmartLock mttContract = getMttContract(ContractAddress,web3j,credentials);
        System.out.println("変更前:"+getWeb3jInfo.getEthBanlance(web3j,ContractAddress));
        //System.out.println("変更値:"+Convert.toWei("1", Convert.Unit.ETHER));
        System.out.println("Valid:"+mttContract.isValid());
        System.out.println("UnLockIng:"+mttContract.isUnlocking().send());
        mttContract.pay(new BigInteger(String.valueOf(Convert.toWei("1", Convert.Unit.ETHER)))).send();
        System.out.println("変更後:"+getWeb3jInfo.getEthBanlance(web3j,ContractAddress));
    }


    private MWSmartLock getMttContract(String ContractAddress,Web3j web3j,Credentials credentials) throws Exception {
        MWSmartLock contract = MWSmartLock.load(ContractAddress,web3j,credentials,new BigInteger("0"),Contract.GAS_LIMIT);
        return contract;
    }
}
