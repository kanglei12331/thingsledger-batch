package com.microworld.thingsledger.thingsledger.tasklet;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.math.BigInteger;
import java.util.concurrent.ExecutionException;

/**
 * 取引操作クラス
 */
public class EthSendTransaction {


    /**
     *
     */
    private void doEthSendTransaction() throws Exception {
//        String fileName = WalletUtils.generateNewWalletFile(
//                "mwthingsledger",
//                ResourceUtils.getFile("classpath:privateKey/loginKey"));
        Admin web3j = Admin.build(new HttpService("https://rpc.testnet.ludos.one"));
        String address="0xfba860f91eb2e45008e7b8a3d2cc57dfe8bc6a0f";
        String toAddress="0xaf1522C811A4aD74332D89DEA9e3615c30d007B9";
        BigInteger gasPrice= new BigInteger("0");
        BigInteger gasLimit= Contract.GAS_LIMIT;
        BigInteger value =  new BigInteger(String.valueOf(Convert.toWei("1", Convert.Unit.ETHER)));
        Credentials credentials = WalletUtils.loadCredentials(
                "mwthingsledger",
                "src/main/resources/privateKey/loginKey");
        //nonceランダム数の取得
        BigInteger nonce = getNonce(web3j,address);
        //取引対象の取得
        RawTransaction rawTransaction = getTransactionObject(nonce,gasPrice,gasLimit,toAddress,value);
        //サイン
        byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
        String hexValue = Numeric.toHexString(signedMessage);
        org.web3j.protocol.core.methods.response.EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(hexValue).sendAsync().get();
        String transactionHash = ethSendTransaction.getTransactionHash();
        System.out.println("hash:"+transactionHash);


    }

    /**
     * nonceを獲得する
     * @param web3j
     */
    private BigInteger getNonce(Web3j web3j,String address) throws ExecutionException, InterruptedException {
        EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(
                address, DefaultBlockParameterName.LATEST).sendAsync().get();

        BigInteger nonce = ethGetTransactionCount.getTransactionCount();
        return nonce;
    }
    //取引対象の取得

    /**
     *
     * @param nonce
     * @param gasPrice
     * @param gasLimit
     * @param toAddress
     * @param value
     * @return 取引対象
     */
    private  RawTransaction getTransactionObject(BigInteger nonce,BigInteger gasPrice,BigInteger gasLimit,String toAddress,BigInteger value){
        RawTransaction rawTransaction  = RawTransaction.createEtherTransaction(
                nonce,gasPrice, gasLimit, toAddress, value);
        return rawTransaction;
    }

    public static void main(String args[]) throws Exception {
        new EthSendTransaction().doEthSendTransaction();
    }
}
