package com.microworld.thingsledger.thingsledger.tasklet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.admin.Admin;
import org.web3j.protocol.core.*;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlockNumber;
import org.web3j.protocol.core.methods.response.EthCall;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Component
public class getWeb3jInfo {
    final private String ENDPOINT = "http://localhost:8080";
    private String APIKEY = "";

    private static long blockNumber = 0;

    private static long blockNum = 0;

    public static void main(String args[]) throws Exception {
        Web3j web3 = getWeb3j("https://rpc.testnet.ludos.one");
        System.out.println(getEthBanlance(web3,"0xfba860f91eb2e45008e7b8a3d2cc57dfe8bc6a0f"));

    }
    @Scheduled(fixedDelay = 5000)
    public void Web3jInfoPost() throws Exception {

        String userAddress = "0x527270e894e85774c9766eb8dd9e7918857d88b6";
        Web3j web3 = getWeb3j("https://rpc.testnet.ludos.one");
        String getEthBanlance = getEthBanlance(web3,userAddress);
        //Contract erc20Contract = getERC20Contract(web3, "0xe64b47931f28f89Cc7A0C6965Ecf89EaDB4975f5", userAddress);
        //getERC20Balance(web3,erc20Contract,userAddress,8);
        Integer value = checkIfExpired(userAddress,"0x527270e894e85774c9766eb8dd9e7918857d88b6",web3);
        BigInteger currentValue=getBlockNumber(web3).getBlockNumber();
        if(currentValue.compareTo(BigInteger.valueOf(value)) > 0){
            setTBParameters(false);
            //金額更新
            updateTBParameters(getEthBanlance, false, currentValue);
        }else{
            setTBParameters(true);
            //金額更新
            updateTBParameters(getEthBanlance, true, currentValue);
        }
        //取引履歴の更新
        transactionsInfoUpload("0x527270e894e85774c9766eb8dd9e7918857d88b6");
        close(web3);

    }
    //即時残高取得すること
    public void updateTBParameters(String value, boolean flag, BigInteger currentValue) throws JSONException, JsonProcessingException {
        //クライアントについて
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        WebResource resource = client.resource(ENDPOINT + "/api/v1/RASPBERRY_PI_DEMO_TOKEN/attributes");

        String req1;
        Map<String,Object> map = new HashMap<>();
        map.put("balance",value);
        map.put("isUsing",flag);
        map.put("blockNumber",currentValue);

        req1 = new ObjectMapper().writeValueAsString(map);
        System.out.println(req1);


        //リクエストする
        ClientResponse response = resource.accept(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, req1);
        //レスポンスする
        System.out.println(response.getStatus());
        if(response.getStatus() ==200) {
            System.out.println("金額更新しました");
        }
    }

    /**
     *
     * @param conTractAddress
     */
    private void transactionsInfoUpload(String conTractAddress) throws JsonProcessingException, org.codehaus.jettison.json.JSONException, JSONException {
        //クライアントについて
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        WebResource resource = client.resource("http://52.197.119.213:8000/transactions");

        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("address",conTractAddress);

        String req1;
        Map<String,Object> map = new HashMap<>();

        //リクエストする
        ClientResponse response = resource.queryParams(queryParams).accept(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);
        org.codehaus.jettison.json.JSONObject resp = response.getEntity(org.codehaus.jettison.json.JSONObject.class);
        System.out.println(resp.getString("docs"));
        List<JSONObject> result=jsonTransferArrayMap((String) resp.getString("docs"));
        int size = result.size();
        long getBlockNumber=0;
        if(size >0){
            WebResource resourceUploadToTL = client.resource("http://localhost:8080/api/v1/RASPBERRY_PI_DEMO_TOKEN/telemetry");
            for (int i=0;i<size;i++){
                JSONObject job = result.get(size-(i+1));
                getBlockNumber = job.getLong("blockNumber");
                //blockNumberを判断する
                if(getBlockNumber > blockNumber){
                    map.put("From","「"+job.getString("from"));
                    map.put("To",job.getString("to")+"」");

                    map.put("blockNumber",getBlockNumber);
                    map.put("value",job.getString("value"));
                    map.put("state","Error".equals(job.getString("error"))?"error":"ok");
                    map.put("hashTx","「"+job.getString("_id")+"」");

                    req1 = new ObjectMapper().writeValueAsString(map);
                    System.out.println(req1);

                    //リクエストする
                    ClientResponse responseUpload = resourceUploadToTL.accept(MediaType.APPLICATION_JSON)
                            .post(ClientResponse.class, req1);
                    blockNumber = getBlockNumber;

                }
            }
        }

    }

    public void setTBParameters(boolean controlFlg) throws JSONException, JsonProcessingException {
        //クライアントについて
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        WebResource resource = client.resource(ENDPOINT + "/api/plugins/rpc/oneway/537632e0-e4cb-11e9-9c2d-f733bf51bbd4");

        String req1;
        Map<String,Object> map = new HashMap<>();
        map.put("method","setLockState");
        map.put("params",controlFlg);
        req1 = new ObjectMapper().writeValueAsString(map);
        System.out.println(req1);

        if (APIKEY.equals("")){
            login();
        }
        //リクエストする
        ClientResponse response = resource.header("X-Authorization","Bearer "+APIKEY)
                .header("charset","UTF-8").accept(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, req1);
        //レスポンスする
        System.out.println(response.getStatus());
        if(response.getStatus() ==200) {
            System.out.println("鍵を開けました");
        }else{
            login();
        }
    }

    public void login() throws JSONException, JsonProcessingException {
        //クライアントについて
        ClientConfig cc = new DefaultClientConfig();
        Client client = Client.create(cc);
        WebResource resource = client.resource(ENDPOINT+"/api/auth/login");

        String req1;
        Map<String,Object> map = new HashMap<>();
        map.put("username","tenant@thingsboard.org");
        map.put("password","tenant");
        req1 = new ObjectMapper().writeValueAsString(map);
        System.out.println(req1);


        //リクエストする
        ClientResponse response = resource.header("charset","UTF-8").accept(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, req1);
        //レスポンスする
        System.out.println(response);
        if(response.getStatus() ==200) {
            JSONObject output = new JSONObject(response.getEntity(String.class));
            APIKEY = output.get("token").toString();
            System.out.println("GET NEW APIKEY: "+ APIKEY);
        }
    }

    /**
     * 连接网络
     * @param netUrl
     * @return
     */
    public static Web3j getWeb3j(String netUrl){
        Admin web3j = Admin.build(new HttpService(netUrl));
        return web3j;
    }

    /**
     * 关闭网络
     * @param web3j
     */
    public static void close(Web3j web3j) {
        web3j.shutdown();
    }

    /**
     * 查询以太币账户余额
     * @throws IOException
     */
    public static String getEthBanlance(Web3j web3j,String userAddress) throws IOException {
        //获取指定钱包的以太币余额
        BigInteger integer=web3j.ethGetBalance(userAddress, DefaultBlockParameterName.LATEST).send().getBalance();
        System.out.println(integer);

        //eth默认会部18个0这里处理比较随意
        String decimal = toDecimal(18,integer);
        System.out.println(decimal);


        //因为是按18位来算的,所有在倒数18位前面加 小数点
		/*String str = integer.toString();
		String decimal = toDecimal(18,str);
		System.out.println(decimal);*/
        return decimal;
    }

    /**
     * 获得 erc20 合约 实例
     * @return
     */
    public static Contract getERC20Contract(Web3j web3j, String contractAddress, String userPri){
        BigInteger GAS_PRICE = BigInteger.valueOf(22_000_000_000L);
        BigInteger GAS_LIMIT = BigInteger.valueOf(4_300_000);
        TransactionManager transactionManager = new TransactionSon(web3j,userPri);
        Credentials credentials = Credentials.create(userPri);//获得信任凭证

        MttContract contract = MttContract.load(
                contractAddress, web3j, transactionManager, GAS_PRICE, GAS_LIMIT);
        return contract;
    }


    /**
     * 转换成符合 decimal 的数值
     * @param decimal
     * @param integer
     * @return
     */
    public static String toDecimal(int decimal,BigInteger integer){
//		String substring = str.substring(str.length() - decimal);
        StringBuffer sbf = new StringBuffer("1");
        for (int i = 0; i < decimal; i++) {
            sbf.append("0");
        }
        String balance = new BigDecimal(integer).divide(new BigDecimal(sbf.toString()), 18, BigDecimal.ROUND_DOWN).toPlainString();
        return balance;
    }


    /**
     * 查询erc 20 代币价格
     * @throws Exception
     */
    public static String getERC20Balance(Web3j web3j,Contract contract,String userAddress,int decimal) throws Exception{

//		Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().send();//send 是同步请求
//		Web3ClientVersion web3ClientVersion = web3.web3ClientVersion().sendAsync().get(); //sendAsync(), 使用CompletableFuture（Android上的Future）发送异步请求：
//		String clientVersion = web3ClientVersion.getWeb3ClientVersion();
//		System.out.println(clientVersion);//查看版本信息
        //或者 RxJava Observable：
		/*Web3j web3 = Web3j.build(new HttpService());  // defaults to http://localhost:8545/
		web3.web3ClientVersion().observable().subscribe(x -> {
		    String clientVersion = x.getWeb3ClientVersion();
		    ...
		});*/


        RemoteCall<BigInteger> balanceOf = ((MttContract) contract).balanceOf(userAddress);
        BigInteger send2 = balanceOf.send();
        String result = toDecimal(8, send2);
        System.out.println(result);
        return result;
    }

    public Integer checkIfExpired(String address, String contractAddress,Web3j web3) throws ExecutionException, InterruptedException {
        Function function = new Function("getExpirationBlockNumber",
                new ArrayList<>(),
                Arrays.asList(new TypeReference<Address>() {
                }));

        String encode = FunctionEncoder.encode(function);
        System.out.println("encode:"+encode);
        Transaction ethCallTransaction = Transaction.createEthCallTransaction(address, contractAddress, encode);
        EthCall ethCall = web3.ethCall(ethCallTransaction, DefaultBlockParameterName.LATEST).sendAsync().get();
        String value = ethCall.getResult();
        Integer valueResult = Integer.parseInt(value.substring(2),16);

        return valueResult;
    }


    public EthBlockNumber getBlockNumber( Web3j web3) throws ExecutionException, InterruptedException {
        EthBlockNumber result = new EthBlockNumber();
        result = web3.ethBlockNumber()
                .sendAsync()
                .get();
        return result;
    }

    //jsonがarray[map]に変更されるmethodだ
    private List<JSONObject> jsonTransferArrayMap(String jsonStr) throws JSONException {
        List<JSONObject> result = new ArrayList<>();
        JSONArray json = new JSONArray(jsonStr);
        if(json.length() > 0){
            for(int i=0;i<json.length();i++){
                JSONObject job = json.getJSONObject(i);
                result.add(job);
            }
        }
        return result;


    }
    @Scheduled(fixedDelay = 5000)
    private void blockNumberHashTimeUpload() throws IOException, ExecutionException, InterruptedException {
        Web3j web3 = getWeb3j("https://rpc.testnet.ludos.one");
//        List<EthBlock.TransactionResult> txs = web3.ethGetBlockByNumber(DefaultBlockParameterName.LATEST, true).send().getBlock().getTransactions();
//        txs.forEach(tx -> {
//            EthBlock.TransactionObject transaction = (EthBlock.TransactionObject) tx.get();
//
//            System.out.println(transaction.getFrom());
//        });

        BigInteger currentValue=getBlockNumber(web3).getBlockNumber();
        if(currentValue.longValue() > blockNum){
            DefaultBlockParameter defaultBlockParameter = new DefaultBlockParameterNumber(currentValue);
            Request<?, EthBlock> request = web3.ethGetBlockByNumber(defaultBlockParameter, true);
            EthBlock ethBlock = request.send();
            System.out.println(currentValue);
            System.out.println(ethBlock.getBlock().getHash());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateStr = sdf.format(new Date((ethBlock.getBlock().getTimestamp().longValue()*1000)));
            System.out.println(dateStr);
            System.out.println(System.currentTimeMillis());
            System.out.println(sdf.format(new Date(System.currentTimeMillis())));
            System.out.println(ethBlock.getBlock().getTimestamp().longValue()*1000);
            System.out.println(ethBlock.getBlock().getTransactions().size());
            //クライアントについて
            ClientConfig cc = new DefaultClientConfig();
            Client client = Client.create(cc);
            WebResource resourceUploadToTL = client.resource("http://localhost:8080/api/v1/RASPBERRY_PI_DEMO_TOKEN/telemetry");

            //リクエストする
            String req1;
            Map<String,Object> map = new HashMap<>();
            map.put("blockNUM",currentValue);
            map.put("hash",ethBlock.getBlock().getHash());
            map.put("timesTamp",dateStr);
            map.put("txNum",ethBlock.getBlock().getTransactions().size());
            req1 = new ObjectMapper().writeValueAsString(map);
            System.out.println(req1);

            ClientResponse responseUpload = resourceUploadToTL.accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, req1);
            blockNum = currentValue.longValue();
        }
    }

}
